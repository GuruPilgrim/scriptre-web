package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"unicode"
)

var templates = template.Must(template.ParseFiles("index.html"))
var booknames [][3]string
var bibleData []Bible
var serverData ServerInfo
var langI int
var canonI int

type Canon struct {
	CanonID   string
	CanonName string
	Language  string
	Groups    []string
	Books     []struct {
		ID           string
		Prefix       string
		BookName     string
		AbbrLong     string
		AbbrShort    string
		Group        string
		Weight       int
		Misspellings []string
		FullTitle    struct {
			TitlePrefix string //The first Book of Moses,
			TitleBridge string //Called || To The
			TitleName   string //Genesis
			SubTitle    string //When He was Held Captive in Babylon
			Attribution string //King of Judah
			Authorship  string //Paul
		}
		Chapters []struct {
			ChapterNumber int
			Verses        []int
		}
	}
}

type Bible struct {
	Version     string
	VersionName string
	Revision    string
	Books       []Book
}

type BibleInfo struct {
	Version         string
	VersionName     string
	DisplayName     string
	Revision        string
	Books           []string
	Language        string
	IsSelected      string
	DefaultRevision bool
}

type BibleVersion struct {
	VersionName string
	Revisions   []BibleInfo
	IsSelected  string
}

type Book struct {
	Name     string
	Chapters []Chapter
}

type Chapter struct {
	Number int
	Verses []Verse
}

type Verse struct {
	Reference  Reference
	Language   string
	Subsection string
	Text       string
}

type VerseGroup struct {
	Verses []Verse
}

type Reference struct {
	BibleVersion string
	Book         string
	Chapter      int
	VerseNumber  int
	VerseRange   int
	Canon        string
}

type Language struct {
	Language     string
	LanguageName string
	Dictionary   map[string]string
	Canons       []Canon
}

type ServerInfo struct {
	Title               string
	Bibles              []BibleVersion
	SelectedBible       string
	SelectedBibleInfo   BibleInfo
	PreferredCanon      string
	PreferredBibleOrder []string
	DefaultLanguage     string
	Languages           []Language
}

func handleWeb(w http.ResponseWriter, r *http.Request) {

	//page := template.Must(template.ParseFiles("index.html", "quick_nav.html"))
	page := template.Must(template.ParseFiles("index.html", "options.html", "quick_nav.html"))
	//page := template.New("scriptre")
	page.Execute(w, serverData)
}

func loadFile(file string) (*[]byte, error) {
	fileContents, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	return &fileContents, nil
}

func main() {
	//preload data
	initialize()

	//handlers
	http.HandleFunc("/", handleWeb)
	http.HandleFunc("/v0/", handleAPIBeta)
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("css"))))
	http.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("js"))))

	log.Fatal(http.ListenAndServe(":80", nil))
}

func initialize() {
	fmt.Printf("initializing...\n")

	//load server config
	_, err := os.Stat("server_config.re")
	if err != nil {
		fmt.Printf("Failed to load server configuration file!\n")
		panic(err)
	} else {
		serverConf, _ := os.Open("server_config.re")
		defer serverConf.Close()
		decoder := json.NewDecoder(serverConf)
		err := decoder.Decode(&serverData)
		if err != nil {
			panic(err)
		}
		fmt.Printf("server config: %+v\n", serverData)

	}

	//determine which canon to use
	//canon := "protestant"

	//get available canons
	langs, _ := ioutil.ReadDir("languages")
	for _, lang := range langs {
		if lang.IsDir() {
			thisLang := new(Language)
			thisLang.Language = lang.Name()

			files, _ := ioutil.ReadDir(fmt.Sprintf("languages/%s/", lang.Name()))
			for _, file := range files {
				if strings.HasSuffix(file.Name(), ".lang") {
				} else if strings.HasSuffix(file.Name(), ".dict") {
				} else if strings.HasSuffix(file.Name(), ".canon") {
					rawCanon, err := os.Open(fmt.Sprintf("languages/%s/%s", lang.Name(), file.Name()))
					if err != nil {
						fmt.Printf("error opening file: %v\n", err)
					}
					defer rawCanon.Close()
					byteCanon, _ := ioutil.ReadAll(rawCanon)
					thisCanon := new(Canon)
					json.Unmarshal(byteCanon, &thisCanon)
					thisLang.Canons = append(thisLang.Canons, *thisCanon)
				}
			}
			fmt.Printf("adding language %v\n", thisLang.LanguageName)
			serverData.Languages = append(serverData.Languages, *thisLang)
		}
	}

	//set default language and canon
	for i, lang := range serverData.Languages {
		if lang.Language == serverData.DefaultLanguage {
			langI = i
			for j, canon := range serverData.Languages[langI].Canons {
				if canon.CanonName == serverData.PreferredCanon {
					canonI = j
				}
			}
		}
	}
	fmt.Printf("lang: %v, canon: %v\n", serverData.Languages[langI].LanguageName, serverData.Languages[langI].Canons[canonI].CanonName)

	//set groups
	var thisGroup string
	for _, book := range serverData.Languages[langI].Canons[canonI].Books {
		if book.Group != thisGroup {
			if thisGroup != "" {
				serverData.Languages[langI].Canons[canonI].Groups = append(serverData.Languages[langI].Canons[canonI].Groups, thisGroup)
			}
			thisGroup = book.Group
		}
	}
	serverData.Languages[langI].Canons[canonI].Groups = append(serverData.Languages[langI].Canons[canonI].Groups, thisGroup)
	fmt.Printf("Groups: %+v\n", serverData.Languages[langI].Canons[canonI].Groups)

	//preload bibles
	err = filepath.Walk("./bibles/", func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			_, err := os.Stat(fmt.Sprintf("%s/config.re", path))
			if err == nil {
				bibleConf, _ := os.Open(fmt.Sprintf("%s/config.re", path))
				defer bibleConf.Close()
				decoder := json.NewDecoder(bibleConf)
				thisBibleInfo := new(BibleInfo)
				err := decoder.Decode(&thisBibleInfo)
				if err != nil {
					fmt.Printf("FATAL: Could not read Bible configuration file.\n")
					panic(err)
				}
				if thisBibleInfo.Version == serverData.SelectedBible {
					serverData.SelectedBibleInfo = *thisBibleInfo
					thisBibleInfo.IsSelected = "yes"
				}
				var knownVersion = false
				for i, v := range serverData.Bibles {
					if thisBibleInfo.VersionName == v.VersionName {
						knownVersion = true
						serverData.Bibles[i].Revisions = append(serverData.Bibles[i].Revisions, *thisBibleInfo)
						break
					}
				}
				if !knownVersion {
					thisVersion := new(BibleVersion)
					thisVersion.VersionName = thisBibleInfo.VersionName
					if thisBibleInfo.IsSelected == "yes" {
						thisVersion.IsSelected = "yes"
					}
					thisVersion.Revisions = append(thisVersion.Revisions, *thisBibleInfo)
					serverData.Bibles = append(serverData.Bibles, *thisVersion)
				}
				fmt.Printf("serverinfo: %+v\n", serverData.Bibles)

				fmt.Printf("\nLoading Bible: %s\n", thisBibleInfo.Version)
				var thisBible Bible
				thisBible.Version = thisBibleInfo.Version
				books, _ := ioutil.ReadDir(path)
				for _, bookFile := range books {
					rawBook, _ := os.Open(fmt.Sprintf("%s/%s", path, bookFile.Name()))
					filename := bookFile.Name()
					var thisBook Book
					if filename[len(filename)-6:] == ".p.sfm" {
						thisBook, _ = readPSFM(rawBook)
					} else if filename[len(filename)-4:] == ".sfm" {
						thisBook, _ = readSFM(rawBook)
					} else if filename[len(filename)-4:] == "usfm" {
						thisBook, _ = readUSFM(rawBook)
					}
					thisBible.Books = append(thisBible.Books, thisBook)
				}
				bibleData = append(bibleData, thisBible)
			}
			curDirFiles, _ := ioutil.ReadDir(path)
			for _, _ = range curDirFiles {
			}
		}
		return nil
	})
	if err != nil {
	}
	for _, v := range serverData.Bibles {
		fmt.Printf("%s\n", v.VersionName)
	}
	fmt.Printf("bibles:\n")
	for _, b := range bibleData {
		fmt.Printf("%s\n", b.Version)
	}

	//reorder Bible info
	if len(serverData.PreferredBibleOrder) > 0 {
		var tempVersionInfo []BibleVersion
		//order preferred Bibles first
		for _, versionString := range serverData.PreferredBibleOrder {
			for _, version := range serverData.Bibles {
				if strings.ToLower(versionString) == strings.ToLower(version.VersionName) {
					tempVersionInfo = append(tempVersionInfo, version)
				}
			}
		}
		completeBibleInfo := tempVersionInfo
		//fill in the rest of the Bibles
		for _, version := range serverData.Bibles {
			var versionAdded = false
			for _, v := range tempVersionInfo {
				if version.VersionName == v.VersionName {
					versionAdded = true
				}
			}
			if !versionAdded {
				completeBibleInfo = append(completeBibleInfo, version)
			}
		}
		//apply new ordered info
		serverData.Bibles = completeBibleInfo
	}

	/*
		bibleLangs, _ := ioutil.ReadDir("./bibles/")

		for _, lang := range bibleLangs {
			bibles, _ := ioutil.ReadDir(fmt.Sprintf("./bibles/%s/", lang.Name()))
			for _, bible := range bibles {
				var thisBible Bible
				thisBible.Version = bible.Name()
				fmt.Printf("\nLoading Bible: %s\n", thisBible.Version)
				books, _ := ioutil.ReadDir(fmt.Sprintf("./bibles/%s/%s/", lang.Name(), bible.Name()))
				for _, bookFile := range books {
					rawBook, _ := os.Open(fmt.Sprintf("./bibles/%s/%s/%s", lang.Name(), bible.Name(), bookFile.Name()))
					filename := bookFile.Name()
					var thisBook Book
					//fmt.Printf("%s", filename)
					//fmt.Printf("%s\n", filename[len(filename)-4:])
					if filename[len(filename)-4:] == ".sfm" {
						thisBook, _ = readSFM(rawBook)
					}
					if filename[len(filename)-4:] == "usfm" {
						thisBook, _ = readUSFM(rawBook)
					}
					thisBible.Books = append(thisBible.Books, thisBook)
				}
				bibleData = append(bibleData, thisBible)
			}
		}
	*/
	//for _, bible := range bibleData {
	//	fmt.Printf("%+v\n", bible.Version)
	//}
	fmt.Printf("\nfinished initializing.\n")
}

func readPSFM(rawBook *os.File) (Book, error) {
	scanner := bufio.NewScanner(rawBook)

	var thisBook Book
	var curChapter = new(Chapter)
	var curVerse = new(Verse)
	var activeVerse = false

	cleanVerse := func(v Verse) Verse {
		/*
			drop := func(s string, rem string) string {
				cleanString := strings.ReplaceAll(s, fmt.Sprintf("%s*", rem), "")
				cleanString = strings.ReplaceAll(cleanString, fmt.Sprintf("%s ", rem), "")
				cleanString = strings.ReplaceAll(cleanString, rem, "")
				return cleanString
			}
			replace := func(s string, remStart string, remEnd string, replacementStart string, replacementEnd string) string {
				cleanString := strings.ReplaceAll(s, remEnd, replacementEnd)
				cleanString = strings.ReplaceAll(cleanString, remStart, replacementStart)
				remStart = strings.TrimSpace(remStart)
				cleanString = strings.ReplaceAll(cleanString, remStart, replacementStart)
				return cleanString
			}

			//remove word info
			remove := func(s string, rem string) string {
				if strings.Contains(s, rem) {
					regcomp := fmt.Sprintf("`%s.*%s\\*`")
					regWord, _ := regexp.Compile(regcomp)
					for hasWord := true; hasWord; hasWord = regWord.MatchString(s) {
						diff := len(rem)
						iStart := strings.Index(s, rem)
						iEnd := strings.Index(s, fmt.Sprintf("%s*", rem))
						prefix := strings.TrimSpace(s[:iStart])
						suffix := strings.TrimSpace(s[iEnd+diff+1:])
						cleanText := fmt.Sprintf("%s %s", prefix, suffix)
						s = cleanText
					}
				}
				return s
			}
			v.Text = strings.ReplaceAll(v.Text, "[", "")
			v.Text = strings.ReplaceAll(v.Text, "]", "")
			v.Text = strings.ReplaceAll(v.Text, "//", "")
			v.Text = strings.ReplaceAll(v.Text, "~", "")

			regWord, _ := regexp.Compile(`\\w.*\|.*\\w\*`)
			if strings.Contains(v.Text, "\\w") {
				for hasWord := true; hasWord; hasWord = regWord.MatchString(v.Text) {
					if strings.Contains(v.Text, "|") && strings.Contains(v.Text, "\\w*") {
						iStart := strings.Index(v.Text, "\\w")
						iEnd := strings.Index(v.Text, "\\w*")
						prefix := v.Text[:iStart]
						word := v.Text[iStart : iEnd+3]
						suffix := v.Text[iEnd+3:]
						pipeIndex := strings.Index(word, "|")
						newWord := word[3:pipeIndex]
						cleanText := fmt.Sprintf("%s%s%s", prefix, newWord, suffix)
						v.Text = cleanText
					} else {
						//newWord := thisWord[3:iEnd]
					}
				}
			}
			regWordPlus, _ := regexp.Compile(`\\+w.*\|.*\\+w\*`)
			if strings.Contains(v.Text, "\\+w") {
				for hasWord := true; hasWord; hasWord = regWordPlus.MatchString(v.Text) {
					if strings.Contains(v.Text, "|") && strings.Contains(v.Text, "\\+w*") {
						iStart := strings.Index(v.Text, "\\+w")
						iEnd := strings.Index(v.Text, "\\+w*")
						prefix := v.Text[:iStart]
						word := v.Text[iStart : iEnd+4]
						suffix := v.Text[iEnd+4:]
						pipeIndex := strings.Index(word, "|")
						newWord := word[3:pipeIndex]
						cleanText := fmt.Sprintf("%s%s%s", prefix, newWord, suffix)
						v.Text = cleanText
					} else {
						//newWord := thisWord[3:iEnd]
					}
				}
			}

			v.Text = replace(v.Text, "\\addpn ", "\\addpn*", "[", "]")
			v.Text = replace(v.Text, "\\add ", "\\add*", "[", "]")

			v.Text = drop(v.Text, "\\nd")
			v.Text = drop(v.Text, "\\ord")
			v.Text = drop(v.Text, "\\png")
			v.Text = drop(v.Text, "\\pn")
			v.Text = drop(v.Text, "\\rq")
			v.Text = drop(v.Text, "\\va")
			v.Text = drop(v.Text, "\\vp")
			v.Text = drop(v.Text, "\\qac")
			v.Text = drop(v.Text, "\\qt")
			v.Text = drop(v.Text, "\\qs")
			v.Text = drop(v.Text, "\\sig")
			v.Text = drop(v.Text, "\\sls")
			v.Text = drop(v.Text, "\\tl")
			v.Text = drop(v.Text, "\\wj")
			v.Text = drop(v.Text, "\\em")
			v.Text = drop(v.Text, "\\it")
			v.Text = drop(v.Text, "\\bit")
			v.Text = drop(v.Text, "\\no")
			v.Text = drop(v.Text, "\\sc")
			v.Text = drop(v.Text, "\\sup")
			v.Text = drop(v.Text, "\\litl")
			v.Text = drop(v.Text, "\\lik")
			v.Text = drop(v.Text, "\\liv")
			v.Text = drop(v.Text, "\\bk")    //quoted book title
			v.Text = drop(v.Text, "\\dc")    //deuterocanonical additions
			v.Text = drop(v.Text, "\\+dc")   //deuterocanonical additions
			v.Text = drop(v.Text, "\\k")     //keyword
			v.Text = drop(v.Text, "\\ndx")   //subject index entry
			v.Text = drop(v.Text, "\\wg")    //word in greek (greek word list entry)
			v.Text = drop(v.Text, "\\wh")    //word in hebrew (hebrew word list entry)
			v.Text = drop(v.Text, "\\wa")    //word in aramaic (aramaic word list entry)
			v.Text = remove(v.Text, "\\fig") //illustrations
			v.Text = remove(v.Text, "\\f")   //footnote
			v.Text = remove(v.Text, "\\fe")  //footnote endnote content
			v.Text = remove(v.Text, "\\fv")  //footnote verse number
			v.Text = remove(v.Text, "\\fdc") //footnote deuterocanonical content
			v.Text = remove(v.Text, "\\x")   //cross reference
			v.Text = remove(v.Text, "\\pro") //pronunciation

			if strings.Contains(v.Text, "\\f +") {
				indexOfFootnote := strings.Index(v.Text, "\\f +")
				v.Text = v.Text[:indexOfFootnote]
			}
				if strings.Contains(v.Text, "\\f*") {
					remove(v.Text, "\\f")
				}
		*/

		return v
	}

	/*
		stripMarker := func(line string, marker string) {
			cleanString := strings.TrimPrefix(marker, line)
			cleanString = strings.TrimSpace(cleanString)
			if activeVerse {
				curVerse.Text = fmt.Sprintf("%s %s", curVerse.Text, cleanString)
			}
		}

		addText := func(text string) {
			text = strings.TrimSpace(text)
			if strings.Contains(text, " ") && len(text) > 4 && len(curChapter.Verses) > 0 {
				firstSpaceIndex := strings.Index(text, " ")
				//curVerse.Text = fmt.Sprintf("%s %s", strings.TrimSpace(curVerse.Text), text[firstSpaceIndex:])
				curChapter.Verses[len(curChapter.Verses)-1].Text = fmt.Sprintf("%s %s", curChapter.Verses[len(curChapter.Verses)-1].Text, text[firstSpaceIndex:])
			}
		}
	*/

	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), "\\v") || strings.HasPrefix(scanner.Text(), "\\p") {
			if activeVerse && len(curChapter.Verses) > 0 {
				curChapter.Verses[len(curChapter.Verses)-1] = cleanVerse(curChapter.Verses[len(curChapter.Verses)-1])
			}
			activeVerse = false
		}
		if strings.TrimSpace(scanner.Text()) == "" {
			if activeVerse {
				curChapter.Verses[len(curChapter.Verses)-1] = cleanVerse(curChapter.Verses[len(curChapter.Verses)-1])
			}
			activeVerse = false
		}
		if strings.HasPrefix(scanner.Text(), "\\h1") {
			if thisBook.Name == "" {
				thisBook.Name = scanner.Text()[4:]
				thisBook.Name = strings.TrimSpace(thisBook.Name)
				//thisBook.Name = strings.ReplaceAll(thisBook.Name, " ", "_")
				if thisBook.Name[0:1] == "1" {
					thisBook.Name = fmt.Sprintf("I %s", thisBook.Name[2:])
				}
				if thisBook.Name[0:1] == "2" {
					thisBook.Name = fmt.Sprintf("II %s", thisBook.Name[2:])
				}
				if thisBook.Name[0:1] == "3" {
					thisBook.Name = fmt.Sprintf("III %s", thisBook.Name[2:])
				}

				for _, book := range serverData.Languages[langI].Canons[canonI].Books {
					if strings.ToLower(thisBook.Name) == fmt.Sprintf("%s %s", strings.ToLower(book.Prefix), strings.ToLower(book.BookName)) {
						thisBook.Name = fmt.Sprintf("%s %s", book.Prefix, book.BookName)
					} else if strings.ToLower(thisBook.Name) == strings.ToLower(book.BookName) {
						thisBook.Name = book.BookName
					}
				}

				fmt.Printf("%s _ ", thisBook.Name)
			}
			/*
				} else if strings.HasPrefix(scanner.Text(), "\\id") {
					//TODO use this to match book to canon using standard USFM book abbreviations
				} else if strings.HasPrefix(scanner.Text(), "\\usfm") {
				} else if strings.HasPrefix(scanner.Text(), "\\ide") {
				} else if strings.HasPrefix(scanner.Text(), "\\sts") {
				} else if strings.HasPrefix(scanner.Text(), "\\rem") {
				} else if strings.HasPrefix(scanner.Text(), "\\toca") {
				} else if strings.HasPrefix(scanner.Text(), "\\toc") {
					//TODO use this to add book titles
				} else if strings.HasPrefix(scanner.Text(), "\\ib") { //introduction blank line
				} else if strings.HasPrefix(scanner.Text(), "\\iex") { //introduction explanatory text
				} else if strings.HasPrefix(scanner.Text(), "\\ie") { //introduction end
				} else if strings.HasPrefix(scanner.Text(), "\\ili") { //introduction list item
				} else if strings.HasPrefix(scanner.Text(), "\\imi") { //indented introduction flush left
				} else if strings.HasPrefix(scanner.Text(), "\\imte") { //introduction major title ending
				} else if strings.HasPrefix(scanner.Text(), "\\imt") { //major title
				} else if strings.HasPrefix(scanner.Text(), "\\imq") { //introduction margin quote - flush left
				} else if strings.HasPrefix(scanner.Text(), "\\im") { //introduction margin - flush left
				} else if strings.HasPrefix(scanner.Text(), "\\ipi") { //indented introduction paragraph
				} else if strings.HasPrefix(scanner.Text(), "\\ipq") { //introduction paragraph quote
				} else if strings.HasPrefix(scanner.Text(), "\\ipr") { //introduction paragraph right-aligned
				} else if strings.HasPrefix(scanner.Text(), "\\ip") { //introduction paragraph
				} else if strings.HasPrefix(scanner.Text(), "\\iqt") { //introduction quoted text
				} else if strings.HasPrefix(scanner.Text(), "\\iq") { //introduction poetic line
				} else if strings.HasPrefix(scanner.Text(), "\\is") { //section heading
				} else if strings.HasPrefix(scanner.Text(), "\\ior") { //introduction outline reference range
				} else if strings.HasPrefix(scanner.Text(), "\\iot") { //introduction outline title
				} else if strings.HasPrefix(scanner.Text(), "\\io") { //introduction outline entry
				} else if strings.HasPrefix(scanner.Text(), "\\mte") { //major title at ending
				} else if strings.HasPrefix(scanner.Text(), "\\mt") { //major title
				} else if strings.HasPrefix(scanner.Text(), "\\ms") { //major section heading
					//TODO use this to add section headings
				} else if strings.HasPrefix(scanner.Text(), "\\mr") { //major section reference range
				} else if strings.HasPrefix(scanner.Text(), "\\sr") { //section reference range
				} else if strings.HasPrefix(scanner.Text(), "\\sp") { //speaker identification
				} else if strings.HasPrefix(scanner.Text(), "\\sd") { //semantic division (amount of space to add between verses)
				} else if strings.HasPrefix(scanner.Text(), "\\s") { //section heading
					//TODO use this to add section headings
				} else if strings.HasPrefix(scanner.Text(), "\\r") { //Parallel passage reference(s)
				} else if strings.HasPrefix(scanner.Text(), "\\d") { //descriptive title
					//TODO use this to add section headings
				} else if strings.HasPrefix(scanner.Text(), "\\ca") { //alternate chapter number
				} else if strings.HasPrefix(scanner.Text(), "\\cl") { //chapter label
				} else if strings.HasPrefix(scanner.Text(), "\\cp") { //published chapter character
				} else if strings.HasPrefix(scanner.Text(), "\\cd") { //chapter description
				} else if strings.HasPrefix(scanner.Text(), "\\va") { //\\va*   alternate verse number
				} else if strings.HasPrefix(scanner.Text(), "\\pmo") { //embedded text opening
					stripMarker(scanner.Text(), "\\pmo")
				} else if strings.HasPrefix(scanner.Text(), "\\pmc") { //embedded text closing
					stripMarker(scanner.Text(), "\\pmc")
				} else if strings.HasPrefix(scanner.Text(), "\\pmr") { //embedded text refrain
					stripMarker(scanner.Text(), "\\pmr")
				} else if strings.HasPrefix(scanner.Text(), "\\pm") { //embedded text paragraph
				} else if strings.HasPrefix(scanner.Text(), "\\po") { //opening of an epistle / letter
				} else if strings.HasPrefix(scanner.Text(), "\\pr") { //paragraph right-aligned
				} else if strings.HasPrefix(scanner.Text(), "\\pi") { //paragraph indented
				} else if strings.HasPrefix(scanner.Text(), "\\pb") { //explicit page break
				} else if strings.HasPrefix(scanner.Text(), "\\ph") { //paragraph with hanging indent
				} else if strings.HasPrefix(scanner.Text(), "\\p") { //paragraph
					//TODO detect if line has other content
				} else if strings.HasPrefix(scanner.Text(), "\\mi") { //indented flush left paragraph no first line indent
				} else if strings.HasPrefix(scanner.Text(), "\\m") { //continuation (margin) paragraph
				} else if strings.HasPrefix(scanner.Text(), "\\cls") { //closure of an epistle / letter
				} else if strings.HasPrefix(scanner.Text(), "\\nb") { //no break (with previous paragraph)
				} else if strings.HasPrefix(scanner.Text(), "\\b") { //blank line
				} else if strings.HasPrefix(scanner.Text(), "\\qa") { //acrostic heading
				} else if strings.HasPrefix(scanner.Text(), "\\qc") { //centered poetic line
					addText(scanner.Text())
				} else if strings.HasPrefix(scanner.Text(), "\\qd") { //Hebrew note (at end of psalm)
				} else if strings.HasPrefix(scanner.Text(), "\\qm") { //embedded text poetic line
					addText(scanner.Text())
				} else if strings.HasPrefix(scanner.Text(), "\\qr") { //poetic line right-aligned
					addText(scanner.Text())
				} else if strings.HasPrefix(scanner.Text(), "\\q") { //poetic line (followed by number like "\q1")
					addText(scanner.Text())
				} else if strings.HasPrefix(scanner.Text(), "\\lh") { //list header
				} else if strings.HasPrefix(scanner.Text(), "\\lim") { //embedded list item
				} else if strings.HasPrefix(scanner.Text(), "\\lf") { //list footer
				} else if strings.HasPrefix(scanner.Text(), "\\li") { //list item
				} else if strings.HasPrefix(scanner.Text(), "\\tr") { //table row start
				} else if strings.HasPrefix(scanner.Text(), "\\thr") { //table column heading right-aligned
				} else if strings.HasPrefix(scanner.Text(), "\\th") { //table column heading
				} else if strings.HasPrefix(scanner.Text(), "\\tcr") { //table cell right-aligned
				} else if strings.HasPrefix(scanner.Text(), "\\tc") { //table cell
				} else if strings.HasPrefix(scanner.Text(), "\\lit") { //liturgical note
				} else if strings.HasPrefix(scanner.Text(), "\\pb") { //explicit page break
			*/
		} else if strings.HasPrefix(scanner.Text(), "\\c") {
			if len(curChapter.Verses) > 0 {
				curChapter.Verses[len(curChapter.Verses)-1] = cleanVerse(curChapter.Verses[len(curChapter.Verses)-1])
			}
			if curChapter.Number > 0 {
				thisBook.Chapters = append(thisBook.Chapters, *curChapter)
			}
			number := strings.TrimSpace(scanner.Text()[3:])
			chapter := new(Chapter)
			newint, _ := strconv.Atoi(number)
			chapter.Number = newint
			curChapter = chapter
		} else if strings.HasPrefix(scanner.Text(), "\\v") {
			activeVerse = true
			curVerse = new(Verse)
			thisReference := new(Reference)
			var verseNumberString string
			var textIndex int
			textIndex = 5
			verseNumberString = string(scanner.Text()[3])
			if unicode.IsNumber(rune(scanner.Text()[4])) {
				textIndex = 6
				verseNumberString = fmt.Sprintf("%s%s", verseNumberString, string(scanner.Text()[4]))
			}
			if unicode.IsNumber(rune(scanner.Text()[5])) {
				textIndex = 7
				verseNumberString = fmt.Sprintf("%s%s", verseNumberString, string(scanner.Text()[5]))
			}
			//thisReference.BibleVersion = thisBible.Version
			thisReference.Book = thisBook.Name
			thisReference.Chapter = curChapter.Number
			thisReference.VerseNumber, _ = strconv.Atoi(verseNumberString)
			thisReference.Canon = "protestant"
			curVerse.Reference = *thisReference
			curVerse.Text = scanner.Text()[textIndex:]
			curVerse.Language = "en"
			curVerse.Subsection = "all"
			curChapter.Verses = append(curChapter.Verses, *curVerse)
		} else if !strings.HasPrefix(scanner.Text(), "\\") && activeVerse == true {
			if strings.TrimSpace(scanner.Text()) != "" && len(curChapter.Verses) > 0 {
				curChapter.Verses[len(curChapter.Verses)-1].Text = fmt.Sprintf("%s %s", curChapter.Verses[len(curChapter.Verses)-1].Text, scanner.Text())
			}
		}
	}
	if len(curChapter.Verses) > 0 {
		curChapter.Verses[len(curChapter.Verses)-1] = cleanVerse(curChapter.Verses[len(curChapter.Verses)-1])
	}
	thisBook.Chapters = append(thisBook.Chapters, *curChapter)
	return thisBook, nil
}

func readSFM(rawBook *os.File) (Book, error) {
	scanner := bufio.NewScanner(rawBook)

	var thisBook Book
	thisBook.Name = strings.TrimSuffix(strings.Replace(fmt.Sprintf("%s", rawBook.Name()), "_", " ", -1), ".sfm")
	var curChapter = new(Chapter)
	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), "\\c") {
			if curChapter.Number > 0 {
				thisBook.Chapters = append(thisBook.Chapters, *curChapter)
			}
			number := scanner.Text()[3:]
			chapter := new(Chapter)
			chapter.Number, _ = strconv.Atoi(number)
			curChapter = chapter
		} else if strings.HasPrefix(scanner.Text(), "\\v") {
			thisVerse := new(Verse)
			thisReference := new(Reference)
			var verseNumberString string
			var textIndex int
			textIndex = 5
			verseNumberString = string(scanner.Text()[3])
			if unicode.IsNumber(rune(scanner.Text()[4])) {
				textIndex = 6
				verseNumberString = fmt.Sprintf("%s%s", verseNumberString, string(scanner.Text()[4]))
			}
			if unicode.IsNumber(rune(scanner.Text()[5])) {
				textIndex = 7
				verseNumberString = fmt.Sprintf("%s%s", verseNumberString, string(scanner.Text()[5]))
			}
			//thisReference.BibleVersion = thisBible.Version
			thisReference.Book = thisBook.Name
			thisReference.Chapter = curChapter.Number
			thisReference.VerseNumber, _ = strconv.Atoi(verseNumberString)
			thisReference.Canon = "protestant"
			thisVerse.Reference = *thisReference
			thisVerse.Text = scanner.Text()[textIndex:]
			thisVerse.Language = "en"
			thisVerse.Subsection = "all"
			curChapter.Verses = append(curChapter.Verses, *thisVerse)
		}
	}
	thisBook.Chapters = append(thisBook.Chapters, *curChapter)
	return thisBook, nil
}

func readUSFM(rawBook *os.File) (Book, error) {
	scanner := bufio.NewScanner(rawBook)

	var thisBook Book
	var curChapter = new(Chapter)
	var curVerse = new(Verse)
	var activeVerse = false

	cleanVerse := func(v Verse) Verse {
		drop := func(s string, rem string) string {
			cleanString := strings.ReplaceAll(s, fmt.Sprintf("%s*", rem), "")
			cleanString = strings.ReplaceAll(cleanString, fmt.Sprintf("%s ", rem), "")
			cleanString = strings.ReplaceAll(cleanString, rem, "")
			return cleanString
		}
		replace := func(s string, remStart string, remEnd string, replacementStart string, replacementEnd string) string {
			cleanString := strings.ReplaceAll(s, remEnd, replacementEnd)
			cleanString = strings.ReplaceAll(cleanString, remStart, replacementStart)
			remStart = strings.TrimSpace(remStart)
			cleanString = strings.ReplaceAll(cleanString, remStart, replacementStart)
			return cleanString
		}

		//remove word info
		remove := func(s string, rem string) string {
			if strings.Contains(s, rem) {
				regcomp := fmt.Sprintf("`%s.*%s\\*`")
				regWord, _ := regexp.Compile(regcomp)
				for hasWord := true; hasWord; hasWord = regWord.MatchString(s) {
					diff := len(rem)
					iStart := strings.Index(s, rem)
					iEnd := strings.Index(s, fmt.Sprintf("%s*", rem))
					prefix := strings.TrimSpace(s[:iStart])
					suffix := strings.TrimSpace(s[iEnd+diff+1:])
					cleanText := fmt.Sprintf("%s %s", prefix, suffix)
					s = cleanText
				}
			}
			return s
		}
		v.Text = strings.ReplaceAll(v.Text, "[", "")
		v.Text = strings.ReplaceAll(v.Text, "]", "")
		v.Text = strings.ReplaceAll(v.Text, "//", "")
		v.Text = strings.ReplaceAll(v.Text, "~", "")

		regWord, _ := regexp.Compile(`\\w.*\|.*\\w\*`)
		if strings.Contains(v.Text, "\\w") {
			for hasWord := true; hasWord; hasWord = regWord.MatchString(v.Text) {
				if strings.Contains(v.Text, "|") && strings.Contains(v.Text, "\\w*") {
					iStart := strings.Index(v.Text, "\\w")
					iEnd := strings.Index(v.Text, "\\w*")
					prefix := v.Text[:iStart]
					word := v.Text[iStart : iEnd+3]
					suffix := v.Text[iEnd+3:]
					pipeIndex := strings.Index(word, "|")
					newWord := word[3:pipeIndex]
					cleanText := fmt.Sprintf("%s%s%s", prefix, newWord, suffix)
					v.Text = cleanText
				} else {
					//newWord := thisWord[3:iEnd]
				}
			}
		}
		regWordPlus, _ := regexp.Compile(`\\+w.*\|.*\\+w\*`)
		if strings.Contains(v.Text, "\\+w") {
			for hasWord := true; hasWord; hasWord = regWordPlus.MatchString(v.Text) {
				if strings.Contains(v.Text, "|") && strings.Contains(v.Text, "\\+w*") {
					iStart := strings.Index(v.Text, "\\+w")
					iEnd := strings.Index(v.Text, "\\+w*")
					prefix := v.Text[:iStart]
					word := v.Text[iStart : iEnd+4]
					suffix := v.Text[iEnd+4:]
					pipeIndex := strings.Index(word, "|")
					newWord := word[3:pipeIndex]
					cleanText := fmt.Sprintf("%s%s%s", prefix, newWord, suffix)
					v.Text = cleanText
				} else {
					//newWord := thisWord[3:iEnd]
				}
			}
		}

		v.Text = replace(v.Text, "\\addpn ", "\\addpn*", "[", "]")
		v.Text = replace(v.Text, "\\add ", "\\add*", "[", "]")
		v.Text = replace(v.Text, "\\+add ", "\\+add*", "[", "]")

		v.Text = drop(v.Text, "\\nd")
		v.Text = drop(v.Text, "\\ord")
		v.Text = drop(v.Text, "\\png")
		v.Text = drop(v.Text, "\\pn")
		v.Text = drop(v.Text, "\\rq")
		v.Text = drop(v.Text, "\\va")
		v.Text = drop(v.Text, "\\vp")
		v.Text = drop(v.Text, "\\qac")
		v.Text = drop(v.Text, "\\qt")
		v.Text = drop(v.Text, "\\qs")
		v.Text = drop(v.Text, "\\sig")
		v.Text = drop(v.Text, "\\sls")
		v.Text = drop(v.Text, "\\tl")
		v.Text = drop(v.Text, "\\wj")
		v.Text = drop(v.Text, "\\em")
		v.Text = drop(v.Text, "\\it")
		v.Text = drop(v.Text, "\\bit")
		v.Text = drop(v.Text, "\\no")
		v.Text = drop(v.Text, "\\sc")
		v.Text = drop(v.Text, "\\sup")
		v.Text = drop(v.Text, "\\litl")
		v.Text = drop(v.Text, "\\lik")
		v.Text = drop(v.Text, "\\liv")
		v.Text = drop(v.Text, "\\bk")    //quoted book title
		v.Text = drop(v.Text, "\\dc")    //deuterocanonical additions
		v.Text = drop(v.Text, "\\+dc")   //deuterocanonical additions
		v.Text = drop(v.Text, "\\k")     //keyword
		v.Text = drop(v.Text, "\\ndx")   //subject index entry
		v.Text = drop(v.Text, "\\wg")    //word in greek (greek word list entry)
		v.Text = drop(v.Text, "\\wh")    //word in hebrew (hebrew word list entry)
		v.Text = drop(v.Text, "\\wa")    //word in aramaic (aramaic word list entry)
		v.Text = remove(v.Text, "\\fig") //illustrations
		v.Text = remove(v.Text, "\\f")   //footnote
		v.Text = remove(v.Text, "\\fe")  //footnote endnote content
		v.Text = remove(v.Text, "\\fv")  //footnote verse number
		v.Text = remove(v.Text, "\\fdc") //footnote deuterocanonical content
		v.Text = remove(v.Text, "\\x")   //cross reference
		v.Text = remove(v.Text, "\\pro") //pronunciation

		if strings.Contains(v.Text, "\\f +") {
			indexOfFootnote := strings.Index(v.Text, "\\f +")
			v.Text = v.Text[:indexOfFootnote]
		}
		/*
			if strings.Contains(v.Text, "\\f*") {
				remove(v.Text, "\\f")
			}
		*/

		return v
	}

	stripMarker := func(line string, marker string) {
		cleanString := strings.TrimPrefix(marker, line)
		cleanString = strings.TrimSpace(cleanString)
		if activeVerse {
			curVerse.Text = fmt.Sprintf("%s %s", curVerse.Text, cleanString)
		}
	}

	addText := func(text string) {
		text = strings.TrimSpace(text)
		if strings.Contains(text, " ") && len(text) > 4 && len(curChapter.Verses) > 0 {
			firstSpaceIndex := strings.Index(text, " ")
			//curVerse.Text = fmt.Sprintf("%s %s", strings.TrimSpace(curVerse.Text), text[firstSpaceIndex:])
			curChapter.Verses[len(curChapter.Verses)-1].Text = fmt.Sprintf("%s %s", curChapter.Verses[len(curChapter.Verses)-1].Text, text[firstSpaceIndex:])
		}
	}

	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), "\\v") || strings.HasPrefix(scanner.Text(), "\\p") {
			if activeVerse && len(curChapter.Verses) > 0 {
				curChapter.Verses[len(curChapter.Verses)-1] = cleanVerse(curChapter.Verses[len(curChapter.Verses)-1])
			}
			activeVerse = false
		}
		if strings.TrimSpace(scanner.Text()) == "" {
			if activeVerse {
				curChapter.Verses[len(curChapter.Verses)-1] = cleanVerse(curChapter.Verses[len(curChapter.Verses)-1])
			}
			activeVerse = false
		}
		if strings.HasPrefix(scanner.Text(), "\\h") {
			if thisBook.Name == "" {
				thisBook.Name = scanner.Text()[3:]
				thisBook.Name = strings.TrimSpace(thisBook.Name)
				//thisBook.Name = strings.ReplaceAll(thisBook.Name, " ", "_")
				if thisBook.Name[0:1] == "1" {
					thisBook.Name = fmt.Sprintf("I%s", thisBook.Name[1:])
				}
				if thisBook.Name[0:1] == "2" {
					thisBook.Name = fmt.Sprintf("II%s", thisBook.Name[1:])
				}
				if thisBook.Name[0:1] == "3" {
					thisBook.Name = fmt.Sprintf("III%s", thisBook.Name[1:])
				}
				fmt.Printf("%s _ ", thisBook.Name)
			}
		} else if strings.HasPrefix(scanner.Text(), "\\id") {
			//TODO use this to match book to canon using standard USFM book abbreviations
		} else if strings.HasPrefix(scanner.Text(), "\\usfm") {
		} else if strings.HasPrefix(scanner.Text(), "\\ide") {
		} else if strings.HasPrefix(scanner.Text(), "\\sts") {
		} else if strings.HasPrefix(scanner.Text(), "\\rem") {
		} else if strings.HasPrefix(scanner.Text(), "\\toca") {
		} else if strings.HasPrefix(scanner.Text(), "\\toc") {
			//TODO use this to add book titles
		} else if strings.HasPrefix(scanner.Text(), "\\ib") { //introduction blank line
		} else if strings.HasPrefix(scanner.Text(), "\\iex") { //introduction explanatory text
		} else if strings.HasPrefix(scanner.Text(), "\\ie") { //introduction end
		} else if strings.HasPrefix(scanner.Text(), "\\ili") { //introduction list item
		} else if strings.HasPrefix(scanner.Text(), "\\imi") { //indented introduction flush left
		} else if strings.HasPrefix(scanner.Text(), "\\imte") { //introduction major title ending
		} else if strings.HasPrefix(scanner.Text(), "\\imt") { //major title
		} else if strings.HasPrefix(scanner.Text(), "\\imq") { //introduction margin quote - flush left
		} else if strings.HasPrefix(scanner.Text(), "\\im") { //introduction margin - flush left
		} else if strings.HasPrefix(scanner.Text(), "\\ipi") { //indented introduction paragraph
		} else if strings.HasPrefix(scanner.Text(), "\\ipq") { //introduction paragraph quote
		} else if strings.HasPrefix(scanner.Text(), "\\ipr") { //introduction paragraph right-aligned
		} else if strings.HasPrefix(scanner.Text(), "\\ip") { //introduction paragraph
		} else if strings.HasPrefix(scanner.Text(), "\\iqt") { //introduction quoted text
		} else if strings.HasPrefix(scanner.Text(), "\\iq") { //introduction poetic line
		} else if strings.HasPrefix(scanner.Text(), "\\is") { //section heading
		} else if strings.HasPrefix(scanner.Text(), "\\ior") { //introduction outline reference range
		} else if strings.HasPrefix(scanner.Text(), "\\iot") { //introduction outline title
		} else if strings.HasPrefix(scanner.Text(), "\\io") { //introduction outline entry
		} else if strings.HasPrefix(scanner.Text(), "\\mte") { //major title at ending
		} else if strings.HasPrefix(scanner.Text(), "\\mt") { //major title
		} else if strings.HasPrefix(scanner.Text(), "\\ms") { //major section heading
			//TODO use this to add section headings
		} else if strings.HasPrefix(scanner.Text(), "\\mr") { //major section reference range
		} else if strings.HasPrefix(scanner.Text(), "\\sr") { //section reference range
		} else if strings.HasPrefix(scanner.Text(), "\\sp") { //speaker identification
		} else if strings.HasPrefix(scanner.Text(), "\\sd") { //semantic division (amount of space to add between verses)
		} else if strings.HasPrefix(scanner.Text(), "\\s") { //section heading
			//TODO use this to add section headings
		} else if strings.HasPrefix(scanner.Text(), "\\r") { //Parallel passage reference(s)
		} else if strings.HasPrefix(scanner.Text(), "\\d") { //descriptive title
			//TODO use this to add section headings
		} else if strings.HasPrefix(scanner.Text(), "\\ca") { //alternate chapter number
		} else if strings.HasPrefix(scanner.Text(), "\\cl") { //chapter label
		} else if strings.HasPrefix(scanner.Text(), "\\cp") { //published chapter character
		} else if strings.HasPrefix(scanner.Text(), "\\cd") { //chapter description
		} else if strings.HasPrefix(scanner.Text(), "\\va") { //\\va*   alternate verse number
		} else if strings.HasPrefix(scanner.Text(), "\\pmo") { //embedded text opening
			stripMarker(scanner.Text(), "\\pmo")
		} else if strings.HasPrefix(scanner.Text(), "\\pmc") { //embedded text closing
			stripMarker(scanner.Text(), "\\pmc")
		} else if strings.HasPrefix(scanner.Text(), "\\pmr") { //embedded text refrain
			stripMarker(scanner.Text(), "\\pmr")
		} else if strings.HasPrefix(scanner.Text(), "\\pm") { //embedded text paragraph
		} else if strings.HasPrefix(scanner.Text(), "\\po") { //opening of an epistle / letter
		} else if strings.HasPrefix(scanner.Text(), "\\pr") { //paragraph right-aligned
		} else if strings.HasPrefix(scanner.Text(), "\\pi") { //paragraph indented
		} else if strings.HasPrefix(scanner.Text(), "\\pb") { //explicit page break
		} else if strings.HasPrefix(scanner.Text(), "\\ph") { //paragraph with hanging indent
		} else if strings.HasPrefix(scanner.Text(), "\\p") { //paragraph
			//TODO detect if line has other content
		} else if strings.HasPrefix(scanner.Text(), "\\mi") { //indented flush left paragraph no first line indent
		} else if strings.HasPrefix(scanner.Text(), "\\m") { //continuation (margin) paragraph
		} else if strings.HasPrefix(scanner.Text(), "\\cls") { //closure of an epistle / letter
		} else if strings.HasPrefix(scanner.Text(), "\\nb") { //no break (with previous paragraph)
		} else if strings.HasPrefix(scanner.Text(), "\\b") { //blank line
		} else if strings.HasPrefix(scanner.Text(), "\\qa") { //acrostic heading
		} else if strings.HasPrefix(scanner.Text(), "\\qc") { //centered poetic line
			addText(scanner.Text())
		} else if strings.HasPrefix(scanner.Text(), "\\qd") { //Hebrew note (at end of psalm)
		} else if strings.HasPrefix(scanner.Text(), "\\qm") { //embedded text poetic line
			addText(scanner.Text())
		} else if strings.HasPrefix(scanner.Text(), "\\qr") { //poetic line right-aligned
			addText(scanner.Text())
		} else if strings.HasPrefix(scanner.Text(), "\\q") { //poetic line (followed by number like "\q1")
			addText(scanner.Text())
		} else if strings.HasPrefix(scanner.Text(), "\\lh") { //list header
		} else if strings.HasPrefix(scanner.Text(), "\\lim") { //embedded list item
		} else if strings.HasPrefix(scanner.Text(), "\\lf") { //list footer
		} else if strings.HasPrefix(scanner.Text(), "\\li") { //list item
		} else if strings.HasPrefix(scanner.Text(), "\\tr") { //table row start
		} else if strings.HasPrefix(scanner.Text(), "\\thr") { //table column heading right-aligned
		} else if strings.HasPrefix(scanner.Text(), "\\th") { //table column heading
		} else if strings.HasPrefix(scanner.Text(), "\\tcr") { //table cell right-aligned
		} else if strings.HasPrefix(scanner.Text(), "\\tc") { //table cell
		} else if strings.HasPrefix(scanner.Text(), "\\lit") { //liturgical note
		} else if strings.HasPrefix(scanner.Text(), "\\pb") { //explicit page break
		} else if strings.HasPrefix(scanner.Text(), "\\c") {
			if len(curChapter.Verses) > 0 {
				curChapter.Verses[len(curChapter.Verses)-1] = cleanVerse(curChapter.Verses[len(curChapter.Verses)-1])
			}
			if curChapter.Number > 0 {
				thisBook.Chapters = append(thisBook.Chapters, *curChapter)
			}
			number := strings.TrimSpace(scanner.Text()[3:])
			chapter := new(Chapter)
			newint, _ := strconv.Atoi(number)
			chapter.Number = newint
			curChapter = chapter
		} else if strings.HasPrefix(scanner.Text(), "\\v") {
			activeVerse = true
			curVerse = new(Verse)
			thisReference := new(Reference)
			var verseNumberString string
			var textIndex int
			textIndex = 5
			verseNumberString = string(scanner.Text()[3])
			if unicode.IsNumber(rune(scanner.Text()[4])) {
				textIndex = 6
				verseNumberString = fmt.Sprintf("%s%s", verseNumberString, string(scanner.Text()[4]))
			}
			if unicode.IsNumber(rune(scanner.Text()[5])) {
				textIndex = 7
				verseNumberString = fmt.Sprintf("%s%s", verseNumberString, string(scanner.Text()[5]))
			}
			//thisReference.BibleVersion = thisBible.Version
			thisReference.Book = thisBook.Name
			thisReference.Chapter = curChapter.Number
			thisReference.VerseNumber, _ = strconv.Atoi(verseNumberString)
			thisReference.Canon = "protestant"
			curVerse.Reference = *thisReference
			curVerse.Text = scanner.Text()[textIndex:]
			curVerse.Language = "en"
			curVerse.Subsection = "all"
			curChapter.Verses = append(curChapter.Verses, *curVerse)
			//	} else if !strings.HasPrefix(scanner.Text(), "\\") && activeVerse == true {
		} else {
			if strings.TrimSpace(scanner.Text()) != "" && len(curChapter.Verses) > 0 {
				curChapter.Verses[len(curChapter.Verses)-1].Text = fmt.Sprintf("%s %s", curChapter.Verses[len(curChapter.Verses)-1].Text, scanner.Text())
			}
		}
	}
	if len(curChapter.Verses) > 0 {
		curChapter.Verses[len(curChapter.Verses)-1] = cleanVerse(curChapter.Verses[len(curChapter.Verses)-1])
	}
	thisBook.Chapters = append(thisBook.Chapters, *curChapter)
	return thisBook, nil
}
