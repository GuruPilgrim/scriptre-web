function dropVersion() {
  document.getElementById("dropdownVersion").classList.toggle("show");
}
function dropRevision() {
  document.getElementById("dropdownRevision").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

function selectVersion(version){
	  //update the version button
	  versionButton = document.getElementById("btnVersion")
	  versionButton.innerText = version
	  versionDropdown = document.getElementById("dropdownVersion")
	  revisionDropdown = document.getElementById("dropdownRevision")
	  selectedRevision = ""
	
	
	  //repopulate the revision button
	  var bibles = document.getElementById("all_bibles")
	  for (const bible of bibles.children) {
		if (bible.className == "selected_bible") {
		  bible.classList.remove("selected_bible")
		}
		for (const v of bible.children) {
		  if (v.className == "selected_bible") {
		    v.classList.remove("selected_bible")
		  }
		}
	  };
	  for (const bible of bibles.children) {
		  if (bible.id == version) {
		    bible.classList.add("selected_bible")
		    while (revisionDropdown.firstChild) {
			    revisionDropdown.removeChild(revisionDropdown.firstChild);
		    }
		    for (const v of bible.children) {
			    if (selectedRevision == "") {
			      v.classList.add("selected_bible")
			      selectedRevision = v.innerText
			    }
			    var vclone = v.cloneNode(true);
			    revisionDropdown.appendChild(vclone);
		    }
		  }
	  };
	
	  //update the revision button
	  revisionButton = document.getElementById("btnRevision")
	  if (revisionDropdown.childElementCount < 2) {
		  selectedRevision = document.getElementById("dropdownRevision").firstChild.innerText;
	  }
	
	//TODO determine default or user preference revision for this version
	  if (revisionButton.innerText == "") {
		  selectedRevision = document.getElementById("dropdownRevision").firstChild.innerText;
	  }
		  console.log(selectedRevision)
	  revisionButton.innerText = selectedRevision;
	
	//update selected
	for (const link of versionDropdown.children) {
		if (link.innerText == version) {
			link.classList.add("selected_bible")
		} else {
			link.classList.remove("selected_bible")
		}
	}
	for (const link of revisionDropdown.children) {
		if (link.innerText == selectedRevision) {
			link.classList.add("selected_bible")
		} else {
			link.classList.remove("selected_bible")
		}
	}

	switchVersion();
	  
}
function selectRevision(version) {
	for (const ver of document.getElementById("all_bibles").children) {
	    for (const rev of ver.children) {
		if (rev.id == version) {
			rev.classList.add("selected_bible")
		} else {
			rev.classList.remove("selected_bible")
		}
	    }
	}
	for (const rev of document.getElementById("dropdownRevision").children) {
		if (rev.id == version) {
			rev.classList.add("selected_bible")
			document.getElementById("btnRevision").innerText = rev.innerText;
		} else {
			rev.classList.remove("selected_bible")
		}
	}

	switchVersion();
}
